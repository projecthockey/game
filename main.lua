require "player"
require "gameEngine"
require "menu"
require "player1"

local deltaTime

function love.load()
  leftGoalSprite = love.graphics.newImage("sprites/leftGoal.png")
  rightGoalSprite = love.graphics.newImage("sprites/rightGoal.png")
  timer = 0
  timerMin = 5
  gamestate = "menu"
--  player_load()
  player1_load()
  menu_load()
end


function love.update(dt)
  if gamestate == "menu" then
  end
  if gamestate == "game" then
    deltaTime = dt
  
    timer = timer - dt

    if timer < 0 then
      timer = 59
      timerMin = timerMin - 1
    end
--    player_update(dt)
    player1_update(dt)
    
  end
end

function love.draw()
  if gamestate == "menu" then
      menu_draw()
  end
  
  if gamestate == "game" then
    love.graphics.rectangle("fill", 300, 150, 600, 300)
    love.graphics.draw(leftGoalSprite,250,250)
    love.graphics.draw(rightGoalSprite,900,250)
    love.graphics.print(timerMin..":"..math.floor(timer),590,25)
 --   player_draw()
    player1_draw()
  end
end

function love.mousepressed(x, y, button)
  if gamestate == "menu" then
    startButtonPressed(x, y, button)
    exitButtonPressed(x, y, button)
  end 
end

function love.keypressed(key)
  if gamestate == "game" then
    if key == 'f' then
      player1_fire(deltaTime)
    end
  
    if key == "space" then
      player1_fireShot()
    end
  end
end

