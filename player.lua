require "menu"

local rawDistance = 0
  
local speed = 100
  player1sprite = love.graphics.newImage("sprites/player1.png")
  player1 = {}
  player1.x = 500
  player1.y = 300
  player1.width = player1sprite:getWidth()
  player1.height = player1sprite:getHeight()
  player1.direction = 0
  player1.xSpeed = 0
  player1.ySpeed = 0
  player1.score = 0
  
  player1.angle = 90.3
  player1.shots = {}

function player_load()

  player2sprite = love.graphics.newImage("sprites/player2.png")
  ballSprite = love.graphics.newImage("sprites/ball.png")

  player2 = {}
  player2.x = 700
  player2.y = 300
  player2.width = player2sprite:getWidth()
  player2.height = player2sprite:getHeight()
  player2.angle = 0
  player2.xSpeed = 0
  player2.ySpeed = 0
  player2.score = 0
  
  ball = {}
  ball.x = 600
  ball.y = 300
  ball.width = ballSprite:getWidth()
  ball.height = ballSprite:getHeight()
  ball.xSpeed = 0
  ball.ySpeed = 0
  
  shots = {}
  shots.x = 0
  shots.y = 0
  shots.angle = 0
end

function player_update(dt)
  
  player1_rotation()
  player2_rotation()
  
  player_movement(dt, player1.angle, player2.angle)
  player_wallCollision()
  
  
    for i, v in ipairs(player1.shots) do 

    v.x = v.x + math.cos(v.angle) * speed * dt
    v.y = v.y + math.sin(v.angle) * speed * dt
  
  end
end

function player_wallCollision()
  if (player1.x-(player1.width/2) < 300) then
    player1.xSpeed = 0
    player1.x = 300+(player1.width/2)
  elseif (player1.x+(player1.width/2) > 900) then
    player1.xSpeed = 0
    player1.x = 900-(player1.width/2)
  elseif(player1.y-(player1.width/2) < 150) then
    player1.ySpeed = 0
    player1.y = 150+(player1.width/2)
  elseif(player1.y+(player1.width/2) > 450) then 
    player1.ySpeed = 0
    player1.y = 450-(player1.width/2)
  end
  
  if (player2.x-(player2.width/2) < 300) then
    player2.xSpeed = 0
    player2.x = 300+(player2.width/2)
  elseif (player2.x+(player2.width/2) > 900) then
    player2.xSpeed = 0
    player2.x = 900-(player1.width/2)
  elseif(player2.y-(player2.width/2) < 150) then
    player2.ySpeed = 0
    player2.y = 150+(player2.width/2)
  elseif(player2.y+(player2.width/2) > 450) then 
    player2.ySpeed = 0
    player2.y = 450-(player2.width/2)
  end
end







function player_movement(dt, player1Angle, player2Angle)
  player1.x = player1.x + player1.xSpeed*dt
  player1.y = player1.y + player1.ySpeed*dt
  
  player2.x = player2.x + player2.xSpeed*dt
  player2.y = player2.y + player2.ySpeed*dt
  
  
   if love.keyboard.isDown('a') then
    player1.xSpeed = -100
  end
  if love.keyboard.isDown('d') then
    player1.xSpeed = 100
 end
  if love.keyboard.isDown('w') then
    player1.ySpeed = -100
  end
  if love.keyboard.isDown('s') then
    player1.ySpeed = 100
  end
  
  if not love.keyboard.isDown('a') and not love.keyboard.isDown('d') then
    player1.xSpeed = 0
  end
  
  if not love.keyboard.isDown('w') and not love.keyboard.isDown('s') then
    player1.ySpeed = 0
  end
 

  if love.keyboard.isDown("left") then
    player2.xSpeed = -100
  end
  if love.keyboard.isDown('right') then
    player2.xSpeed = 100
 end
  if love.keyboard.isDown('up') then
    player2.ySpeed = -100
  end
  if love.keyboard.isDown("down") then
    player2.ySpeed = 100
  end
  
  if not love.keyboard.isDown("left") and not love.keyboard.isDown("right") then
    player2.xSpeed = 0
  end
  
  if not love.keyboard.isDown("up") and not love.keyboard.isDown("down") then
    player2.ySpeed = 0
  end
end


function player1_rotation()

if love.keyboard.isDown('a') then
    if player1.angle ~= 0 then
        if player_rotate(player1.angle, 0) < 0.1 then
             player1.angle = 0
        else if player_rotate(player1.angle + 0.1, 0) < player_rotate(player1.angle - 0.1, 0) then
            player1.angle = player1.angle + 0.1
        else
            player1.angle = player1.angle - 0.1
        end
      end
    end
  end
  
  if love.keyboard.isDown('d') then
    if player1.angle ~= 90 then
        if player_rotate(player1.angle, 90) < 0.1 then
             player1.angle = 90
        else if player_rotate(player1.angle + 0.1, 90) < player_rotate(player1.angle - 0.1, 90) then
            player1.angle = player1.angle + 0.1
        else
            player1.angle = player1.angle - 0.1
        end
      end
    end
  end
  
    if love.keyboard.isDown('s') then
    if player1.angle ~= 180 then
        if player_rotate(player1.angle, 180) < 0.1 then
             player1.angle = 180
        else if player_rotate(player1.angle + 0.1, 180) < player_rotate(player1.angle - 0.1, 180) then
            player1.angle = player1.angle + 0.1
        else
            player1.angle = player1.angle - 0.1
        end
      end
    end
  end
  
   if love.keyboard.isDown('w') then
    if player1.angle ~= 0.8 then
        if player_rotate(player1.angle, 0.8) < 0.1 then
             player1.angle = 0.8
        else if player_rotate(player1.angle + 0.1, 0.8) < player_rotate(player1.angle - 0.1, 0.8) then
            player1.angle = player1.angle + 0.1
        else
            player1.angle = player1.angle - 0.1
        end
      end
    end
  end
  
  if love.keyboard.isDown('s') and love.keyboard.isDown('d') then
    if player1.angle ~= 135 then
        if player_rotate(player1.angle, 135) < 0.1 then
             player1.angle = 135
        else if player_rotate(player1.angle + 0.1, 135) < player_rotate(player1.angle - 0.1, 135) then
            player1.angle = player1.angle + 0.1
        else
            player1.angle = player1.angle - 0.1
        end
      end
    end
  end

 if love.keyboard.isDown('w') and love.keyboard.isDown('d') then
    if player1.angle ~= 45 then
        if player_rotate(player1.angle, 45) < 0.1 then
             player1.angle = 45
        else if player_rotate(player1.angle + 0.1, 45) < player_rotate(player1.angle - 0.1, 45) then
            player1.angle = player1.angle + 0.1
        else
            player1.angle = player1.angle - 0.1
        end
      end
    end
  end
  
   if love.keyboard.isDown('w') and love.keyboard.isDown('a') then
    if player1.angle ~= -45 then
        if player_rotate(player1.angle, -45) < 0.1 then
             player1.angle = -45
        else if player_rotate(player1.angle + 0.1, -45) < player_rotate(player1.angle - 0.1, -45) then
            player1.angle = player1.angle + 0.1
        else
            player1.angle = player1.angle - 0.1
        end
      end
    end
  end
  
   if love.keyboard.isDown('s') and love.keyboard.isDown('a') then
    if player1.angle ~= -45 then
        if player_rotate(player1.angle, -45) < 0.1 then
             player1.angle = -45
        else if player_rotate(player1.angle + 0.1, -45) < player_rotate(player1.angle - 0.1, -45) then
            player1.angle = player1.angle + 0.1
        else
            player1.angle = player1.angle - 0.1
        end
      end
    end
  end
end




function player2_rotation()
  
  if love.keyboard.isDown("left") then
    if player2.angle ~= 93.5 then
        if player_rotate(player2.angle, 93.5) < 0.1 then
             player2.angle = 93.5
        else if player_rotate(player2.angle + 0.1, 93.5) < player_rotate(player2.angle - 0.1, 93.5) then
            player2.angle = player2.angle + 0.1
        else
            player2.angle = player2.angle - 0.1
        end
      end
    end
  end
  
  if love.keyboard.isDown("right") then
    if player2.angle ~= 90 then
        if player_rotate(player2.angle, 90) < 0.1 then
             player2.angle = 90
        else if player_rotate(player2.angle + 0.1, 90) < player_rotate(player2.angle - 0.1, 90) then
            player2.angle = player2.angle + 0.1
        else
            player2.angle = player2.angle - 0.1
        end
      end
    end
  end
  
    if love.keyboard.isDown("down") then
    if player2.angle ~= 180 then
        if player_rotate(player2.angle, 180) < 0.1 then
             player2.angle = 180
        else if player_rotate(player2.angle + 0.1, 180) < player_rotate(player2.angle - 0.1, 180) then
            player2.angle = player2.angle + 0.1
        else
            player2.angle = player2.angle - 0.1
        end
      end
    end
  end
  
   if love.keyboard.isDown("up") then
    if player2.angle ~= 0.8 then
        if player_rotate(player2.angle, 0.8) < 0.1 then
             player2.angle = 0.8
        else if player_rotate(player2.angle + 0.1, 0.8) < player_rotate(player2.angle - 0.1, 0.8) then
            player2.angle = player2.angle + 0.1
        else
            player2.angle = player2.angle - 0.1
        end
      end
    end
  end
  
  if love.keyboard.isDown("down") and love.keyboard.isDown("right") then
    if player2.angle ~= 135 then
        if player_rotate(player2.angle, 135) < 0.1 then
             player2.angle = 135
        else if player_rotate(player2.angle + 0.1, 135) < player_rotate(player2.angle - 0.1, 135) then
            player2.angle = player2.angle + 0.1
        else
            player2.angle = player2.angle - 0.1
        end
      end
    end
  end

 if love.keyboard.isDown("up") and love.keyboard.isDown("right") then
    if player2.angle ~= 45 then
        if player_rotate(player2.angle, 45) < 0.1 then
             player2.angle = 45
        else if player_rotate(player2.angle + 0.1, 45) < player_rotate(player2.angle - 0.1, 45) then
            player2.angle = player2.angle + 0.1
        else
            player2.angle = player2.angle - 0.1
        end
      end
    end
  end
  
   if love.keyboard.isDown("up") and love.keyboard.isDown("left") then
    if player2.angle ~= -45 then
        if player_rotate(player2.angle, -45) < 0.1 then
             player2.angle = -45
        else if player_rotate(player2.angle + 0.1, -45) < player_rotate(player2.angle - 0.1, -45) then
            player2.angle = player2.angle + 0.1
        else
            player2.angle = player2.angle - 0.1
        end
      end
    end
  end
  
   if love.keyboard.isDown("down") and love.keyboard.isDown("left") then
    if player2.angle ~= -45 then
        if player_rotate(player2.angle, -45) < 0.1 then
             player2.angle = -45
        else if player_rotate(player2.angle + 0.1, -45) < player_rotate(player2.angle - 0.1, -45) then
            player2.angle = player2.angle + 0.1
        else
            player2.angle = player2.angle - 0.1
        end
      end
    end
  end
  
end
  
  
function player1_fire(dt)
  local shots = {}
  
 shots.x = player1.x + player1.width/2 
 shots.y = player1.y + player1.height/2
 
-- shots.angle = player1.angle * math.pi/180
shots.angle = player1.angle * math.pi * 2

 table.insert(player1.shots, shots)
  
end



function player_rotate(radian1, radian2)
   
  if radian1 > radian2 then
    rawDistance = radian1 - radian2
  else
   rawDistance = radian2 - radian1
  end  
  rawDistance = rawDistance % (2*math.pi)
  
  if rawDistance > math.pi then
    return math.pi * 2 - rawDistance
  else
    return rawDistance
  end
end



function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end




function player_draw()
  
  love.graphics.draw(player1sprite, player1.x, player1.y, player1.angle, 1, 1, player1sprite:getWidth()/2, player1sprite:getHeight()/2)
  love.graphics.draw(player2sprite, player2.x, player2.y, player2.angle, 1, 1, player2sprite:getWidth()/2, player2sprite:getHeight()/2)
  love.graphics.draw(ballSprite, ball.x, ball.y)
  
  for i,v in ipairs (player1.shots) do 
    love.graphics.draw(ballSprite, v.x, v.y, v.angle)
   -- print(v.angle)
  end
  
  draw_score()
end




function draw_score()
  love.graphics.print(player1.score, 200, 25)
  love.graphics.print(player2.score, 1000, 25)
end
