  local speed = 100
  
function rotate(vector, angle)
    local cos = math.cos(angle)
    local sin = math.sin(angle)
    local x = vector.x * cos - vector.y * sin
    local y = vector.x * sin + vector.y * cos
    vector.x = x
    vector.y = y
end

function player1_load()
    puckSprite = love.graphics.newImage("sprites/ball.png")
    playerSprite = love.graphics.newImage("sprites/player1.png")
    player = {}
    player.x = 500
    player.y = 300
    player.dx = 10
    player.dy = 10
    player.speed = 50
    player.direction = {}
    player.direction.x = 1
    player.direction.y = 1
    player.angle = 90
    player.width  = puckSprite:getWidth()
    player.height = puckSprite:getHeight()
    player.shots = {}
end

function player1_update(dt)
 
 
 if love.keyboard.isDown('a') then
    player.angle = player.angle + 2 * dt
   end
 if love.keyboard.isDown('d') then
   player.angle = player.angle - 2 * dt
 end
 if love.keyboard.isDown('w') then
  player.direction.x = 1
  player.direction.y = 1
  rotate(player.direction, player.angle)
  player.x = player.x + player.dx * dt
  player.y = player.y + player.dy * dt
  
  player.dx = -player.direction.x * player.speed
  player.dy = -player.direction.y * player.speed
  
 end
 
 
 for i, v in ipairs(player.shots) do 
    v.x = v.x + v.dx * dt
    v.y = v.y + v.dy * dt
    end
end

function player1_fireShot()
  local missile = {}
  player.direction.x = 1
  player.direction.y = 1
  
  rotate(player.direction, player.angle)
  
  missile.x = player.x + player.width * 0.5 * player.direction.x
  missile.y = player.y + player.height * 0.5 * player.direction.y
  
  missile.dx = -player.direction.x * speed
  missile.dy = -player.direction.y * speed
  table.insert(player.shots, missile)
  
end

function player1_draw()
   love.graphics.draw(playerSprite, player.x, player.y, player.angle, 1, 1, player.width/2 + 9, player.height/2 - 10)
  
  for i,v in ipairs (player.shots) do 
    love.graphics.draw(puckSprite, v.x, v.y, 0, 1, 1)
  end
end
