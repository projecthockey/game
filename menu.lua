

function menu_load()
  startButton = {}
  startButton.image = love.graphics.newImage("sprites/startButton.png")
  startButton.x = 450
  startButton.y = 150
  startButton.width = startButton.image:getWidth()
  startButton.height = startButton.image:getHeight()
  
  exitButton = {}
  exitButton.image = love.graphics.newImage("sprites/exitButton.png")
  exitButton.x = 450
  exitButton.y = 300
  exitButton.width = exitButton.image:getWidth()
  exitButton.height = exitButton.image:getHeight()
end

function menu_update()
  
end

function menu_draw()
  love.graphics.draw(startButton.image, startButton.x, startButton.y)
  love.graphics.draw(exitButton.image, exitButton.x, exitButton.y)
end

function startButtonPressed(x, y, button)
if(checkMouseImageCollision(x, y, startButton.x, startButton.y, startButton.width, startButton.height)) then
      gamestate = "game"
    end
end

function exitButtonPressed(x, y, button)
  if(checkMouseImageCollision(x, y, exitButton.x, exitButton.y, exitButton.width, exitButton.height)) then
      love.event.push("quit")
end
end

function checkMouseImageCollision(x1, y1, x2, y2, width, height)
  if x1 > x2 and x1 < x2 + width then 
    if y1 > y2 and y1 < y2 + height then
      return true
    else
      return false
    end
  end
end
  -- http://www.gamefromscratch.com/post/2012/11/18/GameDev-math-recipes-Rotating-to-face-a-point.aspx